angular.module('songhop.controllers', ['ionic', 'songhop.services'])


/*
Controller for the discover page
*/
.controller('DiscoverCtrl', function ($scope, $ionicLoading, $timeout, User, Recommendations) {

    // helper functions for loading
    var showLoading = function () {
        $ionicLoading.show({
            template: '<i class="ion-loading-c"></i>',
            noBackdrop: true
        });
    }

    var hideLoading = function () {
        $ionicLoading.hide();
    }

    // set loading to true first time while we retrieve songs from server.
    showLoading();

    // our first three songs
    //$scope.songs = [
    //   {
    //      "title":"Stealing Cinderella",
    //      "artist":"Chuck Wicks",
    //      "image_small":"https://i.scdn.co/image/d1f58701179fe768cff26a77a46c56f291343d68",
    //      "image_large":"https://i.scdn.co/image/9ce5ea93acd3048312978d1eb5f6d297ff93375d"
    //   },
    //   {
    //      "title":"Venom - Original Mix",
    //      "artist":"Ziggy",
    //      "image_small":"https://i.scdn.co/image/1a4ba26961c4606c316e10d5d3d20b736e3e7d27",
    //      "image_large":"https://i.scdn.co/image/91a396948e8fc2cf170c781c93dd08b866812f3a"
    //   },
    //   {
    //      "title":"Do It",
    //      "artist":"Rootkit",
    //      "image_small":"https://i.scdn.co/image/398df9a33a6019c0e95e3be05fbaf19be0e91138",
    //      "image_large":"https://i.scdn.co/image/4e47ee3f6214fabbbed2092a21e62ee2a830058a"
    //   }
    //];

    //// initialize the current song 
    //$scope.currentSong = angular.copy($scope.songs[0]); 


    // get our first songs
    //Recommendations.getNextSongs()
    //    .then(function(){
    //      $scope.currentSong = Recommendations.queue[0];
    //      Recommendations.playCurrentSong();
    //    });
    //Recommendations.init()
    //.then(function () {
    //    $scope.currentSong = Recommendations.queue[0];
    //    Recommendations.playCurrentSong();
    //    hideLoading();
    //});

    Recommendations.init()
    .then(function () {

        $scope.currentSong = Recommendations.queue[0];

        return Recommendations.playCurrentSong();

    })
    .then(function () {
        // turn loading off
        hideLoading();
        $scope.currentSong.loaded = true;
    });

    //// fired when we favorite / skip a song.
    //$scope.sendFeedback = function (bool) {

    //    // first, add to favorites if they favorited
    //    if (bool) User.addSongToFavorites($scope.currentSong);

    //    // set variable for the correct animation sequence
    //    $scope.currentSong.rated = bool;
    //    $scope.currentSong.hide = true;

    //    //$timeout(function() {
    //    //  // $timeout to allow animation to complete before changing to next song
    //    //  // set the current song to one of our three songs
    //    //  var randomSong = Math.round(Math.random() * ($scope.songs.length - 1));

    //    //  // update current song in scope
    //    //  $scope.currentSong = angular.copy($scope.songs[randomSong]);

    //    //}, 250);

    //    // prepare the next song
    //    Recommendations.nextSong();

    //    $timeout(function () {
    //        // $timeout to allow animation to complete
    //        $scope.currentSong = Recommendations.queue[0];
    //    }, 250);

    //    Recommendations.playCurrentSong();


    //}


    // fired when we favorite / skip a song.
    $scope.sendFeedback = function (bool) {

        // first, add to favorites if they favorited
        if (bool) User.addSongToFavorites($scope.currentSong);

        // set variable for the correct animation sequence
        $scope.currentSong.rated = bool;
        $scope.currentSong.hide = true;

        // prepare the next song
        Recommendations.nextSong();

        // update current song in scope, timeout to allow animation to complete
        $timeout(function () {
            $scope.currentSong = Recommendations.queue[0];
            $scope.currentSong.loaded = false;
        }, 250);

        Recommendations.playCurrentSong().then(function () {
           
            $scope.currentSong.loaded = true;
        });

    }
    // used for retrieving the next album image.
    // if there isn't an album image available next, return empty string.
    $scope.nextAlbumImg = function () {
        if (Recommendations.queue.length > 1) {
            return Recommendations.queue[1].image_large;
        }

        return '';
    }


})


/*
Controller for the favorites page
*/
.controller('FavoritesCtrl', function ($scope,$window, User) {
    // get the list of our favorites from the user service
    $scope.favorites = User.favorites;
    $scope.username = User.username;

    $scope.removeSong = function (song, index) {
        User.removeSongFromFavorites(song, index);
    }
     $scope.openSong = function(song) {
    $window.open(song.open_url, "_system");
  }
})

/*
Controller for our tab bar
*/
.controller('TabsCtrl', function ($scope,User,$window, Recommendations) {
    // stop audio when going to favorites page
    // method to reset new favorites to 0 when we click the fav tab
  $scope.enteringFavorites = function() {
    User.newFavorites = 0;
    Recommendations.haltAudio();
  }
    $scope.leavingFavorites = function () {
        Recommendations.init();
    }

     // expose the number of new favorites to the scope
  $scope.favCount = User.favoriteCount;

  $scope.logout = function() {
    User.destroySession();

    // instead of using $state.go, we're going to redirect.
    // reason: we need to ensure views aren't cached.
    $window.location.href = 'index.html';
  }

})


.controller('SplashCtrl', function($scope,User, $state) {

    
  // attempt to signup/login via User.auth
  $scope.submitForm = function(username, signingUp) {
    User.auth(username, signingUp).then(function(){
      // session is now set, so lets redirect to discover page
      $state.go('tab.discover');

    }, function() {
      // error handling here
      alert('Hmm... try another username.');

    });
  }


});
